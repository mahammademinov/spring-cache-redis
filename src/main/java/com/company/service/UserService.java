package com.company.service;

import com.company.dto.payload.UserPayload;
import com.company.dto.response.UserResponse;
import com.company.entity.User;
import com.company.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public void createUser(UserPayload userPayload) {
        User user = userPayload.toEntity();
        userRepository.save(user);
    }

    public UserResponse findById(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("User not found by id = " + id));
        return UserResponse.fromEntity(user);
    }
}
